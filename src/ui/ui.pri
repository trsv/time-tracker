QT += core widgets

INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/gui/mainwindow.cpp \
    $$PWD/gui/trayicon.cpp

HEADERS += \
    $$PWD/gui/mainwindow.h \
    $$PWD/gui/trayicon.h

FORMS += \
    $$PWD/gui/mainwindow.ui
