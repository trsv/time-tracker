#include "trayicon.h"

#include <QAction>

QMenu *TrayIcon::createMenu() const
{
    auto menu = new QMenu();
    auto viewWindow = new QAction("Развернуть окно", menu);

    connect(viewWindow, &QAction::triggered,
            this, &TrayIcon::viewTriggered);

    menu->addAction(viewWindow);

    auto closeWindow = new QAction("Закрыть", menu);

    connect(closeWindow, &QAction::triggered,
            this, &TrayIcon::closeTriggered);

    menu->addAction(closeWindow);

    return menu;
}

void TrayIcon::onActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason){
    case QSystemTrayIcon::Trigger:
        emit viewTriggered();
        break;
    default:
        break;
    }
}

TrayIcon::TrayIcon(QObject *parent)
    : QSystemTrayIcon(parent)
{
    setIcon(QIcon(":/rsc/time.png"));
    setContextMenu(createMenu());

    connect(this, &TrayIcon::activated,
            this, &TrayIcon::onActivated);

    show();
}
