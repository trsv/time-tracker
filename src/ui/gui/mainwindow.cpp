#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QInputDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_timer(new GlobalTimer)
    , m_tray(this)
{
    ui->setupUi(this);

    ui->viewTaskDesk->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->viewTaskDesk->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->viewTaskDesk, &QWidget::customContextMenuRequested,
            this, &MainWindow::onCustomMenuRequested);

    connect(&m_tray, &TrayIcon::viewTriggered,
            this, &MainWindow::show);
    connect(&m_tray, &TrayIcon::closeTriggered,
            this, &MainWindow::close);

    ui->statusbar->addPermanentWidget(m_timer->label());

    setTaskModel(new TaskModel(this));


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTaskModel(TaskModel *model)
{
    m_modelTask = model;
    ui->viewTaskDesk->setModel(model);
    ui->viewTaskDesk->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->statusbar->addWidget(model->labelTaskCounter());
}

void MainWindow::restore() const
{
    if (m_modelTask) {
        m_modelTask->restore();
    }
}

void MainWindow::onCustomMenuRequested(const QPoint &point)
{
    ui->menuTask->popup(ui->viewTaskDesk->viewport()->mapToGlobal(point));
}


void MainWindow::on_actionAdd_triggered()
{
    bool ok;
    const auto text = QInputDialog::getMultiLineText(this, tr("Create new task"),
                                            tr("Text:"), "", &ok, Qt::Window);
    if (ok && !text.isEmpty()) {
        m_modelTask->addTask(text);
    }
}

void MainWindow::on_actionClose_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, "About",
                       "Icons are taken from the site: https://icons8.ru");
}

void MainWindow::on_actionIn_Work_triggered()
{
    if (m_modelTask) {
//        m_modelTask->setStage(ui->viewTaskDesk->selectionModel()->currentIndex(), Stage::In_Work);
    }
}

void MainWindow::on_actionTest_triggered()
{
    if (m_modelTask) {
//        m_modelTask->setStage(ui->viewTaskDesk->selectionModel()->currentIndex(), Stage::Test);
    }
}

void MainWindow::on_actionDone_triggered()
{
    if (m_modelTask) {
        m_modelTask->setStage(ui->viewTaskDesk->selectionModel()->currentIndex(), Stage::Done);
    }
}

void MainWindow::on_actionPaused_triggered()
{
    if (m_modelTask) {
        m_modelTask->setStage(ui->viewTaskDesk->selectionModel()->currentIndex(), Stage::Undone);
    }
}

void MainWindow::on_actionRemove_triggered()
{
    if (QMessageBox::question(this, "Task Desk", "Remove task?") == QMessageBox::Yes) {

        QMap <int, int> rows;
        for (const auto & index : ui->viewTaskDesk->selectionModel()->selectedIndexes()) {
            rows[index.row()];
        }

        if (!rows.isEmpty()) {
            m_modelTask->removeRows(rows.firstKey(), rows.size());
        }
    }
}

void MainWindow::on_actionTimer_toggled(bool arg1)
{
    if (arg1) {
        m_timer->start();
    } else {
        m_timer->stop();
    }
}

void MainWindow::on_actionReset_triggered()
{
    if (QMessageBox::question(this, "Time Tracker", "Reset timer?") == QMessageBox::Yes) {
        m_timer->reset();
        m_timer->display();
    }
}

void MainWindow::on_actionStart_triggered()
{
    if (m_modelTask) {
        ui->actionTimer->setChecked(true);
        m_modelTask->setTimerEnable(ui->viewTaskDesk->selectionModel()->currentIndex(), true);
    }
}

void MainWindow::on_actionStop_triggered()
{
    if (m_modelTask) {
        m_modelTask->setTimerEnable(ui->viewTaskDesk->selectionModel()->currentIndex(), false);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (isVisible()) {
        event->ignore();
        hide();
    }
}

void MainWindow::on_actionEdit_triggered()
{
    const auto value = QInputDialog::getInt(this, "Изменить таймер", "Минуты");
    if (value) {
        m_timer->edit(value * 60);
    }
}
