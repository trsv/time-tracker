#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "task/model/taskmodel.h"
#include "task/model/globaltimer.h"
#include "trayicon.h"

#include <QMainWindow>
#include <QCloseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Ui::MainWindow *ui;
    TaskModel * m_modelTask;
    GlobalTimer * m_timer;
    TrayIcon m_tray;


public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setTaskModel(TaskModel * model);
    void restore() const;

private slots:
    void onCustomMenuRequested(const QPoint & point);
    void on_actionAdd_triggered();
    void on_actionClose_triggered();
    void on_actionAbout_triggered();
    void on_actionIn_Work_triggered();
    void on_actionTest_triggered();
    void on_actionDone_triggered();
    void on_actionPaused_triggered();
    void on_actionRemove_triggered();
    void on_actionTimer_toggled(bool arg1);
    void on_actionReset_triggered();
    void on_actionStart_triggered();
    void on_actionStop_triggered();

    void closeEvent(QCloseEvent *event) override;
    void on_actionEdit_triggered();
};

#endif // MAINWINDOW_H
