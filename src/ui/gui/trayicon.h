#ifndef TRAYICON_H
#define TRAYICON_H

#include <QSystemTrayIcon>
#include <QMenu>

class TrayIcon : public QSystemTrayIcon
{
    Q_OBJECT

   QMenu * createMenu() const;

   void onActivated(QSystemTrayIcon::ActivationReason reason);

public:
    TrayIcon(QObject * parent);

signals:
    void viewTriggered() const;
    void closeTriggered() const;
};

#endif // TRAYICON_H
