CONFIG += c++17

INCLUDEPATH += $$PWD

SOURCES += main.cpp

include(ui/ui.pri)
include(task/task.pri)
include(common/common.pri)

RESOURCES += \
    $$PWD/rsc.qrc
