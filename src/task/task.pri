QT += core

INCLUDEPATH += $$PWD

SOURCES += \
        $$PWD/control/stage.cpp \
        $$PWD/control/task.cpp \
        $$PWD/model/globaltimer.cpp \
        $$PWD/model/taskcounter.cpp \
        $$PWD/model/tasklist.cpp \
        $$PWD/model/taskmodel.cpp

HEADERS += \
        $$PWD/control/stage.h \
        $$PWD/control/task.h \
        $$PWD/model/globaltimer.h \
        $$PWD/model/taskcounter.h \
        $$PWD/model/tasklist.h \
        $$PWD/model/taskmodel.h
