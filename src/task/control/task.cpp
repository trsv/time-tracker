#include "task.h"

QString Task::text() const
{
    return m_text;
}

void Task::setText(const QString &text)
{
    m_text = text;
}

Task::TaskElapsed Task::elapsed() const
{
    return m_elapsed;
}

Task::TaskNumber Task::number() const
{
    return m_number;
}

void Task::setNumber(const TaskNumber &number)
{
    m_number = number;
}

Stage Task::stage() const
{
    return m_stage;
}

QString Task::stageInline() const
{
    return m_stage.toString();
}

bool Task::setStage(int stage)
{
    return setStage(Stage(stage));
}

bool Task::setStage(const Stage &stage)
{
    const auto isChanged = m_stage != stage;
    m_stage = stage;

    if (!stage.isProcess()) {
        stop();
    }

    return isChanged;
}

QJsonObject Task::toJson() const
{
    return QJsonObject({
                           {"text", text()},
                           {"number", number()},
                           {"elapsed", elapsed()},
                           {"stage", stageInline()}
                       });
}

void Task::initializeFromJson(const QJsonObject &json)
{
    setText(json["text"].toString());

    if (json.contains("number")) {
        setNumber(json["number"].toInt());
    }

    if (json.contains("elapsed")) {
        m_elapsed = json["elapsed"].toInt();
    }

    if (json.contains("stage")) {
        setStage(json["stage"].toString());
    }
}

void Task::start()
{
    if (!m_timerElapsed.isValid()) {
        m_timerElapsed.start();
    }
}

void Task::stop()
{
    if (m_timerElapsed.isValid()) {
        m_elapsed += m_timerElapsed.elapsed();
        m_timerElapsed.invalidate();
    }
}

bool Task::isActive() const
{
    return m_timerElapsed.isValid();
}

Task::Task()
    : m_text()
    , m_stage(Stage::Undone)
    , m_number(0)
    , m_elapsed(0)
{

}
