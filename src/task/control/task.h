#ifndef TASK_H
#define TASK_H

#include "stage.h"

#include <QtCore>

class Task
{
    QString m_text;
    Stage m_stage = Stage::Undone;

    typedef int TaskNumber;
    TaskNumber m_number;

    typedef qint64 TaskElapsed;
    TaskElapsed m_elapsed;

    QElapsedTimer m_timerElapsed;

public:
    Task();

    QString text() const;
    void setText(const QString &text);
    TaskElapsed elapsed() const;
    TaskNumber number() const;
    void setNumber(const TaskNumber &number);
    Stage stage() const;
    QString stageInline() const;
    bool setStage(int stage);
    bool setStage(const Stage &stage);
    QJsonObject toJson() const;
    void initializeFromJson(const QJsonObject & json);
    void start();
    void stop();
    bool isActive() const;
};

#endif // TASK_H
