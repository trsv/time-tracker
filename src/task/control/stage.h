#ifndef STAGE_H
#define STAGE_H

#include <QtCore>

class Stage {
public:
    enum Value : int {
        Undone = 0,
        Done
    };

    Stage() = default;
    constexpr Stage(Stage::Value aStage) : value(aStage) {}
    constexpr Stage(int aStage) : value(Value(aStage)) {}
    Stage(const QString &aStage);


    operator Value() const { return value; }  // Allow switch and comparisons.
    explicit operator bool() = delete;        // Prevent usage: if(fruit)

    constexpr bool operator==(const Stage & a) const { return value == a.value; }
    constexpr bool operator!=(const Stage & a) const { return value != a.value; }

    constexpr bool operator==(const Value & a) const { return value == a; }
    constexpr bool operator!=(const Value & a) const { return value != a; }

    bool isProcess() const;
    static Value fromString(const QString & stage);
    QString toString() const;

private:
    Value value;
};

#endif // STAGE_H
