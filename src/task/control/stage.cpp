#include "stage.h"

static QHash<QString, Stage> const table = {
//    {"Undefined", Stage::Undefined},
//    {"Paused", Stage::Paused},
//    {"In Work", Stage::In_Work},
//    {"Test", Stage::Test},
    {"Undone", Stage::Undone},
    {"Done", Stage::Done}
};

Stage::Stage(const QString &aStage)
    : value(fromString(aStage))
{
}

bool Stage::isProcess() const
{
    return value == Undone;
}

Stage::Value Stage::fromString(const QString &stage)
{
    return table[stage];
}

QString Stage::toString() const
{
    return table.key(value);
}
