#ifndef TASKLIST_H
#define TASKLIST_H

#include "control/task.h"

#include <QtCore>

class TaskList
{
    QVector <Task*> m_tasks;

public:
    TaskList();

    int size() const;
    Task* at(int index) const;
    Task* takeAt(int index);
    Task* last() const;
    void insert(int index, Task * task);

    void dump(const QString & path) const;
    qint64 elapsedTotal() const;
};

#endif // TASKLIST_H
