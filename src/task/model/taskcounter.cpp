#include "taskcounter.h"

QLabel *TaskCounter::label() const
{
    return m_label;
}

void TaskCounter::display()
{
    if (m_counter) {
        m_label->setText(QString("Tasks in work: %1")
                         .arg(m_counter));
    } else {
        m_label->clear();
    }

}

TaskCounter::TaskCounter()
    : m_counter(0)
    , m_label(new QLabel)
{
    display();
}

void TaskCounter::increment()
{
    ++m_counter;
    display();
}

void TaskCounter::decrement()
{
    if (m_counter) {
        --m_counter;
        display();
    }

}

void TaskCounter::process(int value)
{
    if (value) {
        increment();
    } else {
        decrement();
    }
}
