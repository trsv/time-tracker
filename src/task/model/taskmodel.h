#ifndef TASKMODEL_H
#define TASKMODEL_H

#include "tasklist.h"
#include "taskcounter.h"
#include "globaltimer.h"

#include <QtCore>

class TaskModel : public QAbstractTableModel
{
    static constexpr auto PATH_STATE = "resources/tasks.json";
    static const QStringList HEADERS;

    enum Role {
        Timer = Qt::UserRole + 1
    };

    enum Columns {
        Number = 0,
        Stage,
        Text,
        ElapsedTime
    };

    bool m_isDumpEnable = true;
    TaskList m_tasks;
    TaskCounter m_counter;
    GlobalTimer m_timerGlobal;

    bool insertRows(int row, int count, const QModelIndex &parent) override;

    bool addTaskInternal(const QJsonObject &text);
    void setDumpOn();
    void dump();
    bool setDataInternal(const QModelIndex &index, const QVariant &value, int role);

    QVariant fetchDisplayData(const QModelIndex &index) const;
    bool setDisplayData(const QModelIndex &index, const QVariant &value);
    bool setTimerData(const QModelIndex &index, bool isStart);

    QVariant fetchBackgroundData(const QModelIndex &index) const;

public:
    TaskModel(QObject * parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    bool addTask(const QString & task);
    void restore();
    void setStage(const QModelIndex & index, const Stage::Value &stage);
    void setTimerEnable(const QModelIndex & index, bool isEnable);
    qint64 elapsedTotal() const;
    QLabel *labelTaskCounter() const;
    GlobalTimer const * timerGlobal() const;
};

#endif // TASKMODEL_H
