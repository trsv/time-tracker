#include "tasklist.h"
#include "common/io/jsonio.h"

TaskList::TaskList()
{

}

int TaskList::size() const
{
    return m_tasks.size();
}

Task *TaskList::at(int index) const
{
    return (index < size()) ? m_tasks.at(index) : nullptr;
}

Task *TaskList::takeAt(int index)
{
    return (index < size()) ? m_tasks.takeAt(index) : nullptr;
}

Task *TaskList::last() const
{
    return at(size() - 1);
}

void TaskList::insert(int index, Task *task)
{
    m_tasks.insert(index, task);
}

void TaskList::dump(const QString &path) const
{
    QJsonArray array;
    for (const auto & task : m_tasks) {
        array << task->toJson();
    }

    JsonIO().write(path, array);
}

qint64 TaskList::elapsedTotal() const
{
    return std::accumulate(m_tasks.begin(), m_tasks.end(), 0,
                    [] (qint64 a, Task * task)
    {
        return a + task->elapsed();
    });
}
