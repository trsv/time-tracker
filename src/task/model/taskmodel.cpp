#include "taskmodel.h"
#include "common/io/jsonio.h"
#include <QBrush>

const QStringList TaskModel::HEADERS = {
    "№", "Stage", "Task", "Elapsed"
};

bool TaskModel::setDataInternal(const QModelIndex &index, const QVariant &value, int role)
{
    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        return setDisplayData(index, value);
    case Role::Timer: return setTimerData(index, value.toBool());
    default: return false;
    }
}

QVariant TaskModel::fetchDisplayData(const QModelIndex &index) const
{
    if (auto task = m_tasks.at(index.row())) {
        switch (index.column()) {
        case Columns::Number: return task->number();
        case Columns::Stage: return task->stageInline();
        case Columns::Text: return task->text();
        case Columns::ElapsedTime: return QTime::fromMSecsSinceStartOfDay(task->elapsed())
                    .toString();
        }
    }

    return QVariant();
}

bool TaskModel::setDisplayData(const QModelIndex &index, const QVariant &value)
{
    if (auto task = m_tasks.at(index.row())) {
        switch (index.column()) {
        case Columns::Number:
            task->setNumber(value.toULongLong());
            break;
        case Columns::Text:
            task->setText(value.toString());
            break;
        case Columns::Stage:
            if (task->setStage(value.toInt())) {
                m_counter.process(task->stage().isProcess());
            }
            break;
        }
        return true;
    }

    return false;
}

bool TaskModel::setTimerData(const QModelIndex &index, bool isStart)
{
    if (auto task = m_tasks.at(index.row())) {
        (isStart) ? task->start() : task->stop();
        return true;
    }

    return false;
}

QVariant TaskModel::fetchBackgroundData(const QModelIndex &index) const
{
    if (auto task = m_tasks.at(index.row())) {
        switch (task->stage()) {
//        case Stage::Test: return QColor(248, 222, 126);
        case Stage::Done: return QColor(234, 241, 221);
        case Stage::Undone: return (task->isActive()) ? QColor(179, 214, 243) : QVariant();
        default: return QVariant();
        }
    }

    return QVariant();
}

TaskModel::TaskModel(QObject *parent)
    : QAbstractTableModel(parent)
{

}


int TaskModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_tasks.size();
}

int TaskModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return HEADERS.size();
}

QVariant TaskModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        return fetchDisplayData(index);
    case Qt::BackgroundRole: return fetchBackgroundData(index);
    default: return QVariant();
    }
}

bool TaskModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    const auto isSetted = setDataInternal(index, value, role);
    if (isSetted) {
        emit dataChanged(TaskModel::index(index.row(), 0),
                         TaskModel::index(index.row(), columnCount() - 1));
        dump();
    }

    return isSetted;
}

QVariant TaskModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Orientation::Horizontal && role == Qt::DisplayRole) {
        return HEADERS.at(section);
    }

    return QVariant();
}

QLabel *TaskModel::labelTaskCounter() const
{
    return m_counter.label();
}

const GlobalTimer *TaskModel::timerGlobal() const
{
    return &m_timerGlobal;
}

bool TaskModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    for (int i = 0; i < count; i++) {
        auto task = new Task();
        task->setNumber(row + 1);
        m_tasks.insert(row, task);
    }

    endInsertRows();
    dump();
    return true;
}

qint64 TaskModel::elapsedTotal() const
{
    return m_tasks.elapsedTotal();
}

bool TaskModel::addTaskInternal(const QJsonObject & json)
{
    const auto isSuccess = insertRow(rowCount());
    if (isSuccess) {
        if (auto task = m_tasks.last()) {
            task->initializeFromJson(json);

            if (task->stage().isProcess()) {
                m_counter.increment();
            }
        }
    }

    return isSuccess;
}

void TaskModel::setDumpOn()
{
    m_isDumpEnable = true;
    dump();
}

void TaskModel::dump()
{
    if (m_isDumpEnable) {
        m_tasks.dump(PATH_STATE);
    }
}

void TaskModel::restore()
{
    m_isDumpEnable = false;

    const auto & state = JsonIO().readArray(PATH_STATE);
    for (const auto & json : state) {
        addTaskInternal(json.toObject());
    }
    setDumpOn();
}

void TaskModel::setStage(const QModelIndex &index, const Stage::Value &stage)
{
    setData(TaskModel::index(index.row(), Columns::Stage), stage, Qt::DisplayRole);
}

void TaskModel::setTimerEnable(const QModelIndex &index, bool isEnable)
{
    setData(index, isEnable, Role::Timer);
}

bool TaskModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (!count) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);
    for (int i = 0; i < count; i++) {
        if (auto task = m_tasks.takeAt(row)) {
            delete task;
        }
    }

    endRemoveRows();
    return true;
}

Qt::ItemFlags TaskModel::flags(const QModelIndex &index) const
{
    switch (index.column()) {
    case Columns::Text: return QAbstractTableModel::flags(index) | Qt::ItemFlag::ItemIsEditable;
    default: return QAbstractTableModel::flags(index);
    }
}

bool TaskModel::addTask(const QString &task)
{
    m_isDumpEnable = false;
    for (const auto & text : task.split('\n')) {
        addTaskInternal(QJsonObject({
                                        {"text", text}
                                    }));
    }

    setDumpOn();
    return true;
}
