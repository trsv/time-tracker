#ifndef TASKCOUNTER_H
#define TASKCOUNTER_H

#include <QtCore>
#include <QLabel>

class TaskCounter
{
    int m_counter;
    QLabel * m_label;

    void display();

public:
    TaskCounter();

    QLabel *label() const;

    void increment();
    void decrement();
    void process(int value);
};

#endif // TASKCOUNTER_H
