#ifndef GLOBALTIMER_H
#define GLOBALTIMER_H

#include <QtCore>
#include <QLabel>

class GlobalTimer : public QObject
{
    static constexpr auto PATH_STATE = "resources/timer.ini";

    int m_elapsed;

    QTimer m_timer;
    QLabel * m_label;

    void dump() const;
    void restore();

public:
    GlobalTimer(QObject * parent = nullptr);

    void start();
    void stop();
    void reset();
    void edit(int value);
    QLabel * label() const;
    void display();
};

#endif // GLOBALTIMER_H
