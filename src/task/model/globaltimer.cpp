#include "globaltimer.h"

QLabel *GlobalTimer::label() const
{
    return m_label;
}

void GlobalTimer::display()
{
    const auto time = QDateTime::fromMSecsSinceEpoch(++m_elapsed * 1000)
            .toString("hh:mm:ss");
    m_label->setText(QString("Total time: %1")
                     .arg(time));
}

void GlobalTimer::dump() const
{
    QSettings iniFile(PATH_STATE, QSettings::IniFormat);
    iniFile.setValue("elapsed", m_elapsed);
}

void GlobalTimer::restore()
{
    QSettings iniFile(PATH_STATE, QSettings::IniFormat);
    m_elapsed = iniFile.value("elapsed").toInt();

    display();
}

GlobalTimer::GlobalTimer(QObject *parent)
    : QObject(parent)
    , m_elapsed(0)
    , m_label(new QLabel)
{
    QObject::connect(&m_timer, &QTimer::timeout,
                     this, &GlobalTimer::display);
    QObject::connect(&m_timer, &QTimer::timeout,
                     this, &GlobalTimer::dump);

    m_timer.setInterval(1 * 1000);

    restore();
}

void GlobalTimer::start()
{
    if (!m_timer.isActive()) {
        m_timer.start();
    }
}

void GlobalTimer::stop()
{
    if (m_timer.isActive()) {
        m_timer.stop();
    }
}

void GlobalTimer::reset()
{
    m_elapsed = 0;
}

void GlobalTimer::edit(int value)
{
    m_elapsed += value;
    display();
}
