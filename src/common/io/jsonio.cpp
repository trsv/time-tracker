#include "jsonio.h"

void JsonIO::write(const QString &path, const QJsonArray &array) const
{
    FileIO::write(path, QJsonDocument(array).toJson(QJsonDocument::Compact));
}

QJsonArray JsonIO::readArray(const QString &path) const
{
    return QJsonDocument::fromJson(FileIO::read(path)).array();
}
