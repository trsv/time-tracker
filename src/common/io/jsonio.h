#ifndef JSONIO_H
#define JSONIO_H

#include "fileio.h"

#include <QJsonDocument>

class JsonIO : private FileIO
{
public:
    void write(const QString & path, const QJsonArray & array) const;
    QJsonArray readArray(const QString &path) const;
};

#endif // JSONIO_H
