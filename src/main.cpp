#include "ui/gui/mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowIcon(QIcon(":/rsc/time.png"));
    w.show();
    w.restore();
    return a.exec();
}
